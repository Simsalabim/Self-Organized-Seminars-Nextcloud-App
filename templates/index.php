<?php

declare(strict_types=1);

use OCP\Util;

Util::addScript(OCA\SelfOrganizedSeminarsApp\AppInfo\Application::APP_ID, 'main');

?>

<div id="selforganizedseminarsapp"></div>
